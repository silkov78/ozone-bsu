# Generated by Django 4.2.8 on 2024-04-10 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_app', '0008_article_body_be_article_subtitle_be_article_title_be_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='tag',
            field=models.CharField(choices=[('WE', 'О нас'), ('SCI', 'Наука')], default='WE', max_length=3, verbose_name='Тэг'),
        ),
    ]
